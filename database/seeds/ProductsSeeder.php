<?php

use App\product;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $products = [
            [
                'name' => 'Switch',
                'descripcion' => 'Swtich description',
                'price' => 80,
                'image' => '/products/switch.jpg'
            ],
            [
                'name' => 'XBOX',
                'descripcion' => 'Swtich description',
                'price' => 90,
                'image' => '/products/xbox.jpg'
            ],
            [
                'name' => 'PS4',
                'descripcion' => 'Swtich description',
                'price' => 70,
                'image' => '/products/play.jpg'
            ]
            ];

            foreach($products as $product){
                product::create($product);
            }
    }
}
