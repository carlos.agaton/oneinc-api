<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['user_id', 'amount', 'currency'];

    public function products()
    {
        return $this->belongsToMany(product::class, 'payment_product');
    }
}
